#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <chrono>
#include <numeric>

#include "src/Node.h"
#include "src/GreenpeaceTree.h"
using namespace std;

vector<string> split(string line, char separator) {
    vector<string> vect;

    stringstream ss(line);

    string i;

    while (ss >> i)
    {
        vect.push_back(i);

        if (ss.peek() == separator)
            ss.ignore();
    }

    return vect;
}

int main(int argc, char** argv) {
    string fn = argv[1];
    int repeat = 1;
    if(argc == 3) {
        repeat = stoi(argv[2]);
    }

    vector<int> prefix_sizes;
    vector<int> infix_sizes;
    vector<int> heights;
    vector<double> elapsed_file_processing_times;
    vector<double> elapsed_build_times;
    vector<double> elapsed_height_times;
    vector<double> total_times;

    for (int i = 0; i < repeat; ++i) {
        cout << "Greenpeace Tree Decoder(TM) - Running on" << fn << "... (" << i << ")" << endl << endl;
        string prefix_string, infix_string;

        auto start_f = chrono::high_resolution_clock::now();
        fstream f;
        f.open(fn);
        getline(f, prefix_string);
        getline(f, infix_string);
        f.close();

        vector<string> prefix = split(prefix_string, ' ');
        vector<string> infix = split(infix_string, ' ');
        auto finish_f = chrono::high_resolution_clock::now();
        chrono::duration<double> elapsed_file_processing = finish_f - start_f;

        GreenpeaceTree *tree = new GreenpeaceTree();

        // Build
        auto start_b = chrono::high_resolution_clock::now();
        tree->setRoot(tree->build(&prefix, &infix, 0, prefix.size() - 1));
        auto finish_b = chrono::high_resolution_clock::now();
        chrono::duration<double> elapsed_build = finish_b - start_b;

        // Height
        auto start_h = chrono::high_resolution_clock::now();
        int height = tree->getHeight(tree->getRoot());
        auto finish_h = chrono::high_resolution_clock::now();
        chrono::duration<double> elapsed_height = finish_h - start_h;


        cout << "Prefix array size: " << prefix.size() << endl;
        cout << "infix array size: " << infix.size() << endl;
        cout << "Height: " << height << endl;
        cout << "Elapsed Time to process file: " << elapsed_file_processing.count() << endl;
        cout << "Elapsed Time to build: " << elapsed_build.count() << endl;
        cout << "Elapsed Time to discover height: " << elapsed_height.count() << endl;
        cout << "Total Elapsed Time: " << elapsed_height.count() + elapsed_build.count() + elapsed_file_processing.count() << endl;

//        ofstream out(fn + "_result_" + to_string(i));
//        out << height << endl << endl;
//        out << "Prefix array size: " << prefix.size() << endl;
//        out << "infix array size: " << infix.size() << endl;
//        out << "Height: " << height << endl;
//        out << "Elapsed Time to process file: " << elapsed_file_processing.count() << endl;
//        out << "Elapsed Time to build: " << elapsed_build.count() << endl;
//        out << "Elapsed Time to discover height: " << elapsed_height.count() << endl;
//        out << "Total Elapsed Time: " << elapsed_height.count() + elapsed_build.count() + elapsed_file_processing.count() << endl;
//        out.close();

        prefix_sizes.push_back(prefix.size());
        infix_sizes.push_back(infix.size());
        heights.push_back(height);
        elapsed_file_processing_times.push_back(elapsed_file_processing.count());
        elapsed_build_times.push_back(elapsed_build.count());
        elapsed_height_times.push_back(elapsed_height.count());
        total_times.push_back(elapsed_height.count() + elapsed_build.count() + elapsed_file_processing.count());

    }

    if(repeat > 1) {
        ofstream out(fn + "_result");
        out << "Prefix array sizes: ";
        for (auto i = prefix_sizes.begin(); i != prefix_sizes.end(); ++i)
            out << *i << ' ';

        out << endl << "infix array sizes: ";
        for (auto i = infix_sizes.begin(); i != infix_sizes.end(); ++i)
            out << *i << ' ';

        out << endl << "Heights: ";
        for (auto i = heights.begin(); i != heights.end(); ++i)
            out << *i << ' ';

        out << endl << "Elapsed Times to process file: ";
        for (auto i = elapsed_file_processing_times.begin(); i != elapsed_file_processing_times.end(); ++i)
            out << *i << ' ';

        out << endl << "Elapsed Times to build: ";
        for (auto i = elapsed_build_times.begin(); i != elapsed_build_times.end(); ++i)
            out << *i << ' ';

        out << endl << "Elapsed Times to discover height: ";
        for (auto i = elapsed_height_times.begin(); i != elapsed_height_times.end(); ++i)
            out << *i << ' ';

        out << endl << "Total Elapsed Times: ";
        for (auto i = total_times.begin(); i != total_times.end(); ++i)
            out << *i << ' ';

        out << endl << endl << "#Average for: " << repeat << " results: " << endl;
        out << "Prefix array sizes: " << accumulate( prefix_sizes.begin(), prefix_sizes.end(), 0.0)/prefix_sizes.size();
        out << endl << "infix array sizes: " << accumulate( infix_sizes.begin(), infix_sizes.end(), 0.0)/infix_sizes.size();
        out << endl << "Heights: " << accumulate( heights.begin(), heights.end(), 0.0)/heights.size();
        out << endl << "Elapsed Times to process file: " << accumulate( elapsed_file_processing_times.begin(), elapsed_file_processing_times.end(), 0.0)/elapsed_file_processing_times.size();
        out << endl << "Elapsed Times to build: " << accumulate( elapsed_build_times.begin(), elapsed_build_times.end(), 0.0)/elapsed_build_times.size();
        out << endl << "Elapsed Times to discover height: " << accumulate( elapsed_height_times.begin(), elapsed_height_times.end(), 0.0)/elapsed_height_times.size();
        out << endl << "Total Elapsed Times: " << accumulate( total_times.begin(), total_times.end(), 0.0)/total_times.size();
        out.close();
    }

    return 0;
}