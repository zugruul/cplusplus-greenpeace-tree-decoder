//
// Created by 17280105 on 03/09/18.
//

#ifndef GREENPEACE_NODE_H
#define GREENPEACE_NODE_H
#include <iostream>
using namespace std;

class Node {
    private:
        string value;
        Node *left;
        Node *right;
    public:
        Node();
        Node(string value);
        string getValue();
        void setValue(string value);
        Node* getLeft();
        Node* getRight();
        void setLeft(Node *node);
        void setRight(Node *node);
};


#endif //GREENPEACE_NODE_H
