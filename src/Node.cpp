//
// Created by 17280105 on 03/09/18.
//

#include "Node.h"

Node::Node() {
    this->value = "";
    this->left = NULL;
    this->right = NULL;
}

Node::Node(string value) {
    this->value = value;
    this->left = NULL;
    this->right = NULL;
}

string Node::getValue() {
    return value;
}

void Node::setValue(string value){
    this->value = value;
}

Node* Node::getLeft() {
    return left;
}

Node* Node::getRight() {
    return right;
}

void Node::setLeft(Node *node) {
    this->left = node;
}

void Node::setRight(Node *node) {
    this->right = node;
}