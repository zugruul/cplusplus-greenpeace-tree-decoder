//
// Created by 17280105 on 03/09/18.
//

#include "GreenpeaceTree.h"

GreenpeaceTree::GreenpeaceTree(){
    this->root = NULL;
    this->prefix_index = 0;
}

Node* GreenpeaceTree::build(vector<string> *prefix, vector<string> *infix, int from, int to) {
    if (from > to) {
        return NULL;
    }

    Node* node = new Node((*prefix)[prefix_index++]);

    if(from == to) {
        return node;
    }

    int infix_index = search_index(infix, node->getValue(), from, to);

    node->setLeft(build(prefix, infix, from, infix_index-1));
    node->setRight(build(prefix, infix, infix_index+1, to));
    return node;

}

int GreenpeaceTree::search_index(vector<string> *infix, string element, int from, int to) {
    for(int i = from; i <= to; i++) {
        if((*infix)[i] == element) {
            return i;
        }
    }
}

Node* GreenpeaceTree::traverse_prefix(Node* node) {
    if(node == NULL) {
        return NULL;
    }

    cout << node->getValue() << endl;
    traverse_prefix(node->getLeft());
    traverse_prefix(node->getRight());
    return node;
}

Node* GreenpeaceTree::traverse_central(Node* node) {
    if(node == NULL) {
        return NULL;
    }

    traverse_central(node->getLeft());
    cout << node->getValue() << endl;
    traverse_central(node->getRight());
    return node;
}


Node* GreenpeaceTree::getRoot() {
    return this->root;
}

int GreenpeaceTree::getHeight(Node* node) {
    if (node == NULL) {
        return -1;
    } else {
        int leftHeight = getHeight(node->getLeft());
        int rightHeight = getHeight(node->getRight());

        if(leftHeight > rightHeight) {
            return leftHeight + 1;
        } else {
            return rightHeight + 1;
        }
    }
}

void GreenpeaceTree::setRoot(Node* root) {
    this->root = root;
}