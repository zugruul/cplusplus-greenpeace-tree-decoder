#ifndef GREENPEACE_TREE_H
#define GREENPEACE_TREE_H


#include <iostream>
#include <vector>
#include "Node.h"
using namespace std;

class GreenpeaceTree {
    private:
        Node *root;
        int prefix_index;
    public:
        GreenpeaceTree();
        Node* build(vector<string> *prefix, vector<string> *infix, int from, int to);
        int search_index(vector<string> *infix, string element, int from, int to);
        Node* traverse_prefix(Node* node);
        Node* traverse_central(Node* node);
        Node* getRoot();
        int getHeight(Node* node);
    void setRoot(Node* root);
};


#endif //GREENPEACE_TREE_H
