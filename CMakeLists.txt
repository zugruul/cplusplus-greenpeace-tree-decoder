cmake_minimum_required(VERSION 3.12)
project(greenpeace)

set(CMAKE_CXX_STANDARD 14)

add_executable(greenpeace main.cpp src/GreenpeaceTree.cpp src/GreenpeaceTree.h src/Node.cpp src/Node.h)